# Problem

Problem has been described in [this document](/doc/Technical%20Task%20-%20Cron%20Expression%20Parser.pdf).

# Solution

Solution has been written in C# running on .NET Core 5.
Additional solution PoCs (in TypeScript and Kotlin) has been added to [/other](/other) folder, yet 
the one in C# is the final one.

Solution is based on:
* parsing input string can be reduced to:
  * spliting on whitespaces (`\s` or `\t`) (ie: `0 9 * * 5 command`)
  * nested further splitting on commas (ie: `1,1-5,7`)
  * parsing with `(\*|(\d+(-\d+)?))(/\d+)?` regular expression (either star, number or range, followed by optional step)
* observation that any allowed pattern (`*`, `*/n`, `a`, `a/n`, `a-b`, `a-b/n`) can be represented as (reduced to) `a-b/n`;
  for example `a` is acually `a-a/1` while `*/n` is `min-max/n`
* single `a-b/n` expression may generate multiple numbers, single row (minutes, hours, etc.) can 
  consist of many `a-b/n` expressions so we can expand them all with `expressions.flatMap(e => e.Expand()).distinct().sorted()`

# Environment

## Use docker build

In root of this repository build docker image:

```
docker build . -t explain-cs
```

When this suceeded (built, tested and published), application is available from docker image:

```
docker run --rm -ti explain-cs "<expression>"
```

Please note, both of those actions are also available as shell scripts:

```
chmod 744 *.sh
./build.sh
./explain.sh "<expression>
```

## Start interactive session with docker

If you want some more control you can use dotnet SDK tools inside vanilla .NET Core 5 SDK 
container which can be started with:

```
docker run --rm -ti -v $(pwd):/app -w /app mcr.microsoft.com/dotnet/sdk:5.0 bash
```

or

```
chmod 744 *.sh
./docker-session.sh
```

or:

```
bash docker-session.sh
```

## No docker

**NOTE:** it is recommended to use docker (see above), but this paragraph has been added for completeness.

This project requires .NET Core 5 SDK (equivalent of Java JDK) to be built, 
and .NET Core 5 Runtime (equivalent of Java JRE) to be ran.

Same as for Java if you already have SDK (JDK) you technically don't need to install runtime (JRE).

.NET Core SDK 5.0 can be installed from: https://dotnet.microsoft.com/download/dotnet/5.0
but it is heavily recommended to just use docker as it is the quickest way to get it running


# Execution

Once in Docker session (recommended) or with .NET SDK installed, we can:

## Build

```
dotnet build
```

## Test

```
dotnet test
```

## Run

```
dotnet run -p ExplainCron "<expression>"
```

for example

```
dotnet run -p ExplainCron "1/15 0 1,15 * 1-5 /usr/bin/find"
```

produces:

```
root@29cd8761c836:/app# dotnet run -p ExplainCron "1/15 0 1,15 * 1-5 /usr/bin/find"
minute        1 16 31 46
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find
```

## Exploration

If you want to play with it a little bit more aliasing it might be useful,
(also `--no-build` and `--no-restore` reduce startup time, but of course, 
in such case project needs to be built up front)

```
dotnet build -c Release && alias explain="dotnet run --no-build --no-restore -c Release -p $(pwd)/ExplainCron"
```

so now it could be just:

```
explain "1/15 0 1,15 * 1-5 /usr/bin/find"
```

# Elephant in the room

All those languages, most likely, have already a well tested library parsing cron expressions, 
so in real life scenario implementation of feature relying on such expression should start with 
investigation of existing solutions, as most of the time there is not need to reinvent the wheel 
(NOTE: unless there is, of course!).