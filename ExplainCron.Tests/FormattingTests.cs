using System;
using System.Linq;
using ExplainCron.Lib;
using Xunit;

namespace ExplainCron.Tests
{
	// NOTE:
	// seems like formatting of output is part of specification:
	// "The output should be formatted as a table with the field name taking the first
	// 14 columns and the times as a space-separated list following it."
	// So we can test it then (although it feel a little bit like testing implementation details)
	public class FormattingTests
	{
		private const int ColumnSize = 14;

		private const StringSplitOptions SplitOptions =
			StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries;

		[Fact]
		public void OutputFormattingMatchesSpecification()
		{
			var spec = CronSpec.Parse("0/15 9-17/3 10-15 */2 * rm -rf /");
			var output = spec.Render().Split('\n', '\r', SplitOptions);

			string FieldName(string line) => line[..ColumnSize].TrimEnd();
			string FieldValue(string line) => line[ColumnSize..];

			Assert.Equal("minute", FieldName(output[0]));
			Assert.Equal("hour", FieldName(output[1]));
			Assert.Equal("day of month", FieldName(output[2]));
			Assert.Equal("month", FieldName(output[3]));
			Assert.Equal("day of week", FieldName(output[4]));
			Assert.Equal("command", FieldName(output[5]));

			Assert.Equal("0 15 30 45", FieldValue(output[0]));
			Assert.Equal("9 12 15", FieldValue(output[1]));
			Assert.Equal("10 11 12 13 14 15", FieldValue(output[2]));
			Assert.Equal("1 3 5 7 9 11", FieldValue(output[3]));
			Assert.Equal("0 1 2 3 4 5 6", FieldValue(output[4]));
			Assert.Equal("rm -rf /", FieldValue(output[5]));
		}
	}
}
