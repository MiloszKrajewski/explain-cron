using System.Collections.Generic;
using System.Text;

namespace ExplainCron.Lib.Internals
{
	/// <summary>Crontab expression pretty (?) printer.</summary>
	public static class CronSpecRenderer
	{
		/// <summary>Renders <see cref="CronSpec"/> is human-readable form.</summary>
		/// <param name="spec">Crontab entry parsed specification.</param>
		/// <returns>A human-readable crontab entry expansion.</returns>
		public static string Render(CronSpec spec)
		{
			var builder = new StringBuilder();

			void TextLine(string label, string value) =>
				builder.Append(label).AppendLine(value);

			void FreqLine(string label, IEnumerable<FreqSpec> def)
			{
				builder.Append(label);
				foreach (var v in def.Enumerate())
					builder.Append(v).Append(' ');
				builder.AppendLine();
			}

			FreqLine("minute        ", spec.Minutes);
			FreqLine("hour          ", spec.Hours);
			FreqLine("day of month  ", spec.DaysOfMonth);
			FreqLine("month         ", spec.Months);
			FreqLine("day of week   ", spec.DaysOfWeek);
			TextLine("command       ", spec.Command);

			return builder.ToString();
		}
	}
}
