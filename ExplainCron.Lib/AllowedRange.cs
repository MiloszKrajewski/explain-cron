using System;

namespace ExplainCron.Lib
{
	/// <summary>
	/// Range expression, used mainly to concisely pass a pair of numbers: [min,max].
	/// Note: both bound are inclusive.
	/// </summary>
	public class AllowedRange
	{
		/// <summary>Lower bound.</summary>
		public int Min { get; }
		
		/// <summary>Upper bound.</summary>
		public int Max { get; }

		/// <summary>Constructs new Range object.</summary>
		/// <param name="min">Lower bound.</param>
		/// <param name="max">Upper bound.</param>
		public AllowedRange(int min, int max) => (Min, Max) = (min, max);
		
		/// <summary>Checks if range contains given value.</summary>
		/// <param name="value">Tested value.</param>
		/// <returns><c>true</c> if given value is within range, <c>false</c> otherwise.</returns>
		public bool Contains(int value) => value >= Min && value <= Max;

		/// <inheritdoc />
		public override string ToString() => $"[{Min},{Max}]";
	}
}
