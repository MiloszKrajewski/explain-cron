open System
open System.Text
open System.Text.RegularExpressions

module String =
    let join (separator: string) (strings: string seq) = String.Join(separator, strings)

module Option =
    let orElse v o = Option.defaultValue v o

module Domain =
    type Range =
        | Star // *
        | Value of int // a
        | Range of int * int // a-b
    type Step = int option // the step is either there or not
    type Bounds = int * int
    type Freq = { Bounds: Bounds; Text: string; Range: Range; Step: Step }
    type Cron = {
        Minutes: Freq[]; Hours: Freq[]
        DaysOfMonths: Freq[]; Months: Freq[]
        DaysOfWeek: Freq[]
        Command: string list
    }
    
module Freq =
    open Domain
    
    let private freqRx = Regex("""^((\*)|((?<min>\d+)(-(?<max>\d+))?))(\/(?<nth>\d+))?$""")
    
    let validate (freq: Freq) =
        match freq with
            | { Step = Some n } when n < 1 -> None
            | { Range = Value a; Bounds = (lo, hi) } when a < lo || a > hi -> None
            | { Range = Range (a, b); Bounds = (lo, hi) } when a < lo || a > hi || b < lo || b > hi || a > b -> None
            | _ -> Some freq

    let parse (bounds: Bounds) (expression: string): Freq option =
        match freqRx.Match(expression) with
        | m when m.Success ->
            let valueOf (n: string) = match m.Groups.[n] with | g when g.Success -> Some (int g.Value) | _ -> None
            let step = valueOf "nth"
            let respond range = { Bounds = bounds; Text = expression; Step = step; Range = range } |> validate
            match valueOf "min", valueOf "max" with
                | None, None -> Some Star
                | Some min, None -> Some (Value min)
                | Some min, Some max -> Some (Range (min, max))
                | _ -> None
            |> Option.bind respond
        | _ -> None
        
    let parseMany (bounds: Bounds) (expression: string) =
        let freq = expression.Split(',') |> Array.map (parse bounds)
        let allValid = freq |> Array.forall Option.isSome
        if allValid then freq |> Array.map Option.get |> Some else None
       
    let expand (expression: Freq) =
        let or1 = Option.orElse 1
        let min, max, nth = 
            match expression with
            | { Range = Star; Step = n; Bounds = (lo, hi) } -> (lo, hi, or1 n)
            | { Range = Value a; Step = None } -> (a, a, 1)
            | { Range = Value a; Step = Some n; Bounds = (_, hi) } -> (a, hi, n)
            | { Range = Range (a, b); Step = n } -> (a, b, or1 n)
        seq { for i in min..nth..max do i }
    
    let expandMany (expressions: Freq seq) =
        expressions |> Seq.collect expand |> Seq.distinct |> Seq.sort

    let render (expressions: Freq seq) =
        expressions |> expandMany |> Seq.map (sprintf "%d") |> String.join " "

module Cron =
    open Domain

    let (|Minutes|_|) = Freq.parseMany (0, 59)
    let (|Hours|_|) = Freq.parseMany (0, 23)
    let (|DaysOfMonth|_|) = Freq.parseMany (1, 31)
    let (|Months|_|) = Freq.parseMany (1, 12)
    let (|DaysOfWeek|_|) = Freq.parseMany (0, 6)

    let parse (expression: string) =
        match Regex.Split(expression, "\s+") |> List.ofArray with
        | Minutes mi :: Hours ho :: DaysOfMonth dom :: Months mo :: DaysOfWeek dow :: command ->
            {
                Minutes = mi; Hours = ho
                DaysOfMonths = dom; Months = mo
                DaysOfWeek = dow
                Command = command
            }
        | _ -> failwith $"Expression {expression} is not valid"
        
    let render (expression: Cron) =
        StringBuilder()
            .Append("minute        ").AppendLine(expression.Minutes |> Freq.render)
            .Append("hour          ").AppendLine(expression.Hours |> Freq.render)
            .Append("day of month  ").AppendLine(expression.DaysOfMonths |> Freq.render)
            .Append("month         ").AppendLine(expression.Months |> Freq.render)
            .Append("day of week   ").AppendLine(expression.DaysOfWeek |> Freq.render)
            .Append("command       ").AppendLine(expression.Command |> String.join " ")
            .ToString()

[<EntryPoint>]
let main argv =
    argv |> String.join " " |> Cron.parse |> Cron.render |> printfn "%s"
    0