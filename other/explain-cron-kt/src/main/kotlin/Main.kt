fun main(args: Array<String>) {
    val spec = Cron.parse(args.joinToString(" "))

    println("minutes       ${spec.minutes.enumerate().joinToString(" ")}")
    println("hours         ${spec.hours.enumerate().joinToString(" ")}")
    println("days on month ${spec.daysOfMonth.enumerate().joinToString(" ")}")
    println("months        ${spec.months.enumerate().joinToString(" ")}")
    println("days of week  ${spec.daysOfWeek.enumerate().joinToString(" ")}")
}

data class Range(val min: Int, val max: Int)
data class FreqSpec(val min: Int, val max: Int, val nth: Int)
data class CronSpec(
    val minutes: List<FreqSpec>,
    val hours: List<FreqSpec>,
    val daysOfMonth: List<FreqSpec>,
    val months: List<FreqSpec>,
    val daysOfWeek: List<FreqSpec>,
    val command: String
)

object Cron {
    private val spacesRx = Regex("""\s+""")
    private val freqRx = Regex("""^((\*)|(?<min>\d+)(-(?<max>\d+))?)(/(?<nth>\d+))?$""")

    fun parse(text: String): CronSpec {
        val parts = text.split(spacesRx, 6)
        return CronSpec(
            parseList(parts[0], Range(0, 59)), // minutes
            parseList(parts[1], Range(0, 23)), // hours
            parseList(parts[2], Range(1, 31)), // days of month
            parseList(parts[3], Range(1, 12)), // months
            parseList(parts[4], Range(0, 6)), // days of week
            parts[5] // command
        )
    }

    private fun parseList(text: String, allowed: Range): List<FreqSpec> =
        text.split(',').map { parseFreq(it, allowed) }

    private fun parseFreq(text: String, allowed: Range): FreqSpec {
        val match = freqRx.find(text) ?: throw Exception()

        val min = match.tryParseInt("min")
        val max = match.tryParseInt("max")
        val nth = match.tryParseInt("nth")

        return if (min != null && max == null && nth == null) {
            FreqSpec(min, min, 1)
        } else {
            FreqSpec(min ?: allowed.min, max ?: allowed.max, nth ?: 1)
        }
    }
}

fun MatchResult.tryParseInt(name: String): Int? =
    this.groups[name]?.value?.toIntOrNull()

fun FreqSpec.enumerate(): IntProgression =
    this.min..this.max step this.nth

fun List<FreqSpec>.enumerate(): List<Int> =
    this.flatMap { it.enumerate() }.distinct().sorted()