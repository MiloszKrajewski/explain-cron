import assert from "assert";

const rangePattern = /^((\*)|((?<min>\d+)(-(?<max>\d+))?))(\/(?<nth>\d+))?$/;

type Option<T> = T | undefined;

interface AllowedRange { 
    min: number, 
    max: number 
};

interface DefinedRange { 
    min: Option<number>, 
    max: Option<number>, 
    nth: Option<number> 
};

interface CronSpec { 
    mi: number[], // minutes
    ho: number[], // hours
    dom: number[], // days of month
    mo: number[],  // months
    dow: number[], // days of week
    command: string // command
};

function tryParseInt(text: string): Option<number> {
    const result = parseInt(text);
    return Number.isNaN(result) ? undefined : result;
}

function validateRange(text: string, range: DefinedRange, allowed: AllowedRange) {
    const between = (value: Option<number>) => 
        value === undefined || (value >= allowed.min && value <= allowed.max);

    const valid = 
        between(range.min) && 
        between(range.max) &&
        (range.nth ?? 1) >= 0;

    if (!valid) {
        throw new Error(`Expression ${text} is not valid`);
    }
}

function* enumerateRange(
    defined: DefinedRange,
    allowed: AllowedRange
): Generator<number> {
    if (defined.min !== undefined && defined.max === undefined && defined.nth === undefined) {
        // special case for just 'a'
        yield defined.min;
    } else {
        // all other cases: 'a/n', 'a-b', 'a-b/n', '*', and '*/n'
        const [min, max, nth] = [
            defined.min ?? allowed.min, 
            defined.max ?? allowed.max, 
            defined.nth ?? 1
        ];

        for (let i = min; i <= max; i += nth) {
            yield i;
        }
    }
}

function parseRange(text: string, allowed: AllowedRange): number[] {
    const match = rangePattern.exec(text);
    assert(match, `Expression ${text} is not a valid range`);
    const { min, max, nth } = match.groups as any;
    const range = { min: tryParseInt(min), max: tryParseInt(max), nth: tryParseInt(nth) };
    validateRange(text, range, allowed);
    const values = enumerateRange(range, allowed);
    return Array.from(values);
}

function parseList(text: string, allowed: AllowedRange): number[] {
    const ranges = text.split(',');
    const values = ranges.flatMap(r => parseRange(r, allowed));
    const distinct = new Set(values);
    return Array.from(distinct).sort((a, b) => a - b);
}

function parseCron(text: string): CronSpec {
    const [ mi, ho, dom, mo, dow, ...command ] = 
        text.split(/\s+/).filter(s => s !== '');

    return {
        mi: parseList(mi ?? "*", { min: 0, max: 59 }),
        ho: parseList(ho ?? "*", { min: 0, max: 23 }),
        dom: parseList(dom ?? "*", { min: 1, max: 31 }),
        mo: parseList(mo ?? "*", { min: 1, max: 12 }),
        dow: parseList(dow ?? "*", { min: 0, max: 6 }),
        command: command.join(' ')
    };
}

function main(argv: string[]) {
    const spec = parseCron(argv.join(' '));
    console.log(spec);
}

main(process.argv.slice(2));
