﻿#nullable enable

using System;
using System.Linq;
using ExplainCron.Lib;

namespace ExplainCron
{
	class Program
	{
		private static int Main(string[] args)
		{
			try
			{
				Console.WriteLine(CronSpec.Parse(args.Join(" ")).Render());
				return 0;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return -1;
			}
		}
	}
}
